

import java.awt.Color;
import java.util.Hashtable;
import javax.swing.text.*;

/**
 *
 * @author Mitanjo
 */
public class MyDocument {
    private DefaultStyledDocument doc;
    private StyleContext styles;
    private Hashtable runAttr;
    Paragraph[] data = new Paragraph[] {
        new Paragraph("title", new Run[] {
            new Run("none", "Naruhina Love 4ever")
        }),

        new Paragraph("author", new Run[] {
            new Run("none", "Naruto Uzmaky")
        }),

        new Paragraph("edition", new Run[] {
            new Run("none", "Firewolf - Myrmidon")
        }),

        new Paragraph("heading", new Run[] {
            new Run("none", "CHAPTER I")
	}),

        new Paragraph("subtitle", new Run[] {
            new Run("none", "The first meet")
	}),

        new Paragraph("normal", new Run[] {
            new Run("none", "Naruto and Hinata looked at each other for some time in silence:  at last the Hinata says : N-n, naruto-kun!!!")
	}),

        new Paragraph("normal", new Run[] {
            new Run("cquote", "Yosh Hinata!!! What I can do for you ? "),
            new Run("none", "said Naruto.")
	}),
    };

    public MyDocument(DefaultStyledDocument doc, StyleContext styles) {
        this.doc = doc;
        this.styles = styles;
        runAttr = new Hashtable();
    }

    public void loadDocument() {
        createStyles();

        for (int i = 0; i < data.length; i++) {
            Paragraph p = data[i];
            addParagraph(p);
        }
    }

    public void addParagraph(Paragraph p) {
        try {
            Style s = null;
            for(int i = 0; i < p.data.length; i++) {
                Run run = p.data[i];
                s = (Style) runAttr.get(run.attr);
                doc.insertString(doc.getLength(), run.content, s);
            }

            Style ls = styles.getStyle(p.logical);
            doc.setLogicalStyle(doc.getLength() - 1, ls);
            doc.insertString(doc.getLength(), "\n", null);
        }catch(BadLocationException e) {
            System.err.println("Internal error: " + e);
        }
    }

    private void createStyles() {
        Style s = styles.addStyle(null, null);
        runAttr.put("none", s);
        s = styles.addStyle(null, null);       
        StyleConstants.setItalic(s, true);
        StyleConstants.setForeground(s, new Color(153, 153, 102));
        runAttr.put("cquote", s);

        s = styles.addStyle(null, null);
        StyleConstants.setItalic(s, true);
        StyleConstants.setForeground(s, new Color(51, 102, 153));
        runAttr.put("aquote", s);

        Style def = styles.getStyle(StyleContext.DEFAULT_STYLE);

        Style heading = styles.addStyle("heading", def);
        StyleConstants.setFontFamily(heading, "SansSerif");
        StyleConstants.setBold(heading, true);
        StyleConstants.setAlignment(heading, StyleConstants.ALIGN_CENTER);
        StyleConstants.setSpaceAbove(heading, 10);
        StyleConstants.setSpaceBelow(heading, 10);
        StyleConstants.setFontSize(heading, 18);

        // Title
        Style sty = styles.addStyle("title", heading);
        StyleConstants.setFontSize(sty, 32);

        // edition
        sty = styles.addStyle("edition", heading);
        StyleConstants.setFontSize(sty, 16);

        // author
        sty = styles.addStyle("author", heading);
        StyleConstants.setItalic(sty, true);
        StyleConstants.setSpaceBelow(sty, 25);

        // subtitle
        sty = styles.addStyle("subtitle", heading);
        StyleConstants.setSpaceBelow(sty, 35);

        // normal
        sty = styles.addStyle("normal", def);
        StyleConstants.setLeftIndent(sty, 10);
        StyleConstants.setRightIndent(sty, 10);
        StyleConstants.setFontFamily(sty, "SansSerif");
        StyleConstants.setFontSize(sty, 14);
        StyleConstants.setSpaceAbove(sty, 4);
        StyleConstants.setSpaceBelow(sty, 4);
    }

    static class Paragraph {
        String logical;
        Run[] data;
        
        Paragraph(String logical, Run[] data) {
            this.logical = logical;
            this.data = data;
        }
    }

    static class Run {
        String attr;
        String content;

        Run(String attr, String content) {
            this.attr = attr;
            this.content = content;
        }
    }
}
