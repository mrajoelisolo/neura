

import java.awt.*;
import javax.swing.*;
import javax.swing.text.*;

/**
 *
 * @author Mitanjo
 */
public class EditorFrame extends JFrame {
    private JTextComponent editor;

    public EditorFrame() {
        initComponents();
    }

    private void initComponents() {
        setTitle("Editor sample");
        setSize(640, 480);
        setLocationRelativeTo(this);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new BorderLayout());

        editor = createEditor();
        getContentPane().add(editor);
    }

    protected JTextComponent createEditor() {
        StyleContext sc = new StyleContext();
        DefaultStyledDocument doc = new DefaultStyledDocument();
        initDocument(doc, sc);

        JTextPane res = new JTextPane(doc);
        res.setDragEnabled(true);

        return res;
    }

    private void initDocument(DefaultStyledDocument doc, StyleContext sc) {
        MyDocument d = new MyDocument(doc, sc);
        d.loadDocument();
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditorFrame().setVisible(true);
            }
        });
    }
}
