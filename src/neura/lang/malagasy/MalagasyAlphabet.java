package neura.lang.malagasy;

import java.io.Serializable;
import neura.lang.*;

/**
 *
 * @author Mitanjo
 * Ensemble de caractères contenant l'alphabet malagasy, ainsi que les sous ensembles
 * de consonnes(consonants) et de voyelles(vowels)
 */
public class MalagasyAlphabet implements ICharSet, IAlphabeticalCharSet, Serializable {
    private static MalagasyAlphabet instance = new MalagasyAlphabet();

    private MalagasyAlphabet() {}

    public static MalagasyAlphabet getInstance() {
        return instance;
    }

    private CharSet fullAlphabet = new CharSet("abdefghijklmnoprstvyz");
    private CharSet consonants = new CharSet("bdfghjklmnprstvz");
    private CharSet vowels = new CharSet("aeioy");

    public boolean contains(String c, String additional, String except) {
        return fullAlphabet.contains(c, additional, except);
    }

    public CharSet getConsonants() {
        return consonants;
    }

    public CharSet getVowels() {
        return vowels;
    }

    public String getValue() {
        return fullAlphabet.getValue();
    }
}
