package neura.lang.malagasy;

/**
 *
 * @author Mitanjo
 */
public enum EnumMalagasyAutomaton {
    Q1, Q4, Q5, Q6_8, Q7_9_10_11_12, Q13, Q14, Q15, Q16, Q17A, Q17B, Q17C, Q17D, Q18, Q19, Q21, Q23, Q24EXT, Q25EXT;
}
