package neura.lang.malagasy;

import neura.states.EnumState;
import neura.states.IState;
import neura.states.Well;
import neura.lang.*;
import neura.tools.*;

/**
 *
 * @author Mitanjo
 * Classe permettant de générer un automate fini (DFA) (graphe orienté)
 * dont la fonctionnalité correspond aux règles de graphie de la langue malagasy
 */
public class MalagasyAutomatonFactory {
    private static MalagasyAutomatonFactory instance = new MalagasyAutomatonFactory();

    private IState automaton1;
    private IState automaton4;
    private IState automaton5;
    private IState automaton6_8; //Rule 6,8
    private IState automaton7_9_10_11_12;
    private IState automaton13;
    private IState automaton14;
    private IState automaton15;
    private IState automaton16;
    private IState automaton17A;
    private IState automaton17B;
    private IState automaton17C;
    private IState automaton17D;
    private IState automaton18;
    private IState automaton21;
    private IState automaton19;
    private IState automaton23;
    private IState automaton24EXT;
    private IState automaton25EXT;

    private MalagasyAutomatonFactory() {
        initFactory();
    }

    public static MalagasyAutomatonFactory getInstance() {
        return instance;
    }

    private void initFactory() {
        //Rule 1: Ny teny malagasy tsy misy mifarana amin'ny litera 'i'
        IState q0 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q1 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q2 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q0, q1, PropositionFactory.getInstance().createProposition("i", null));
        LinkManager.getInstance().addLink(q0, q0, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, " ", "i"));

        LinkManager.getInstance().addLink(q1, q1, PropositionFactory.getInstance().createProposition("i", null));
        LinkManager.getInstance().addLink(q1, q0, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, "", "i"));
        LinkManager.getInstance().addLink(q1, q2, PropositionFactory.getInstance().createProposition(" ", null));

        automaton1 = q0;

        //Rule 2: Ny teny malagasy tsy misy mifarana amin'ny litera "consonne"
        IState q3 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q4 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q5 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q3, q4, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, null, null));
        LinkManager.getInstance().addLink(q3, q3, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, " ", null));

        LinkManager.getInstance().addLink(q4, q3, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, null, null));
        LinkManager.getInstance().addLink(q4, q4, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, null, null));
        LinkManager.getInstance().addLink(q4, q5, PropositionFactory.getInstance().createProposition(" ", null));

        automaton21 = q3;

        //Rule 19: Ny litera 'i' dia tafiditra ao amin'ny teny malagasy
        IState q6 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q7 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q8 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q9 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q6, q7, PropositionFactory.getInstance().createProposition("i", null));
        LinkManager.getInstance().addLink(q6, q9, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "i"));
        
        LinkManager.getInstance().addLink(q7, q8, PropositionFactory.getInstance().createProposition(" ", null));
        LinkManager.getInstance().addLink(q7, q9, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "i"));

        automaton19 = q6;

        //Rule 23:
        IState q10 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q11 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q10, q10, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, " ", null));
        LinkManager.getInstance().addLink(q10, q11, PropositionFactory.getInstance().createProposition("cquxw", null));

        automaton23 = q10;

        //Rule 20:
        IState q12 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q13 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q14 = StateFactory.getInstance().createState(EnumState.WELL);
        IState q15 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);

        LinkManager.getInstance().addLink(q12, q12, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "y"));
        LinkManager.getInstance().addLink(q12, q13, PropositionFactory.getInstance().createProposition("y", null));

        LinkManager.getInstance().addLink(q13, q14, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, null));
        LinkManager.getInstance().addLink(q13, q15, PropositionFactory.getInstance().createProposition(" ", null));

        automaton4 = q12;

        //Rule 5:
        //Comments: Algorithm correction 12/08/2010
        IState q16 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q17 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q18 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q19 = StateFactory.getInstance().createState(EnumState.WELL);
        IState q20late = StateFactory.getInstance().createState(EnumState.STATE);
        IState q21late = StateFactory.getInstance().createState(EnumState.STATE);

        LinkManager.getInstance().addLink(q16, q16, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "aeoy"));
        LinkManager.getInstance().addLink(q16, q17, PropositionFactory.getInstance().createProposition("aeo", null));
        LinkManager.getInstance().addLink(q16, q20late, PropositionFactory.getInstance().createProposition("y", null));
        LinkManager.getInstance().addLink(q16, q21late, PropositionFactory.getInstance().createProposition("i", null));

        LinkManager.getInstance().addLink(q17, q16, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "aoey"));
        LinkManager.getInstance().addLink(q17, q17, PropositionFactory.getInstance().createProposition("aeo", null));
        LinkManager.getInstance().addLink(q17, q18, PropositionFactory.getInstance().createProposition(" ", null));
        LinkManager.getInstance().addLink(q17, q20late, PropositionFactory.getInstance().createProposition("y", null));

        LinkManager.getInstance().addLink(q20late, q18, PropositionFactory.getInstance().createProposition(" ", null));
        LinkManager.getInstance().addLink(q20late, q19, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, null));

        LinkManager.getInstance().addLink(q21late, q16, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "iy"));
        LinkManager.getInstance().addLink(q21late, q20late, PropositionFactory.getInstance().createProposition("y", null));
        LinkManager.getInstance().addLink(q21late, q19, PropositionFactory.getInstance().createProposition(" ", null));

        automaton5 = q16;

        //Rules 6,8:
        IState q22 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q23 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q24 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q25 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q22, q22, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "n"));
        LinkManager.getInstance().addLink(q22, q23, PropositionFactory.getInstance().createProposition("n", null));
        LinkManager.getInstance().addLink(q22, q24, PropositionFactory.getInstance().createProposition(" ", null));

        LinkManager.getInstance().addLink(q23, q22, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "bp"));
        LinkManager.getInstance().addLink(q23, q24, PropositionFactory.getInstance().createProposition(" ", null));
        LinkManager.getInstance().addLink(q23, q25, PropositionFactory.getInstance().createProposition("bp", null));

        automaton6_8 = q22;

         //Rules 7,9,10,11,12:
        IState q26 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q27 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q28 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q29 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q26, q26, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "m"));
        LinkManager.getInstance().addLink(q26, q27, PropositionFactory.getInstance().createProposition("m", null));
        LinkManager.getInstance().addLink(q26, q28, PropositionFactory.getInstance().createProposition(" ", null));

        LinkManager.getInstance().addLink(q27, q26, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "dgjkt"));
        LinkManager.getInstance().addLink(q27, q28, PropositionFactory.getInstance().createProposition(" ", null));
        LinkManager.getInstance().addLink(q27, q29, PropositionFactory.getInstance().createProposition("dgjkt", null));

        automaton7_9_10_11_12 = q26;

        //Rule 13:
        IState q30 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q31 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q32 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q33 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q34 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q30, q30, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "m"));
        LinkManager.getInstance().addLink(q30, q31, PropositionFactory.getInstance().createProposition("m", null));

        LinkManager.getInstance().addLink(q31, q30, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "d"));
        LinkManager.getInstance().addLink(q31, q32, PropositionFactory.getInstance().createProposition("d", null));

        LinkManager.getInstance().addLink(q32, q30, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "r"));
        LinkManager.getInstance().addLink(q32, q33, PropositionFactory.getInstance().createProposition("r", null));

        LinkManager.getInstance().addLink(q33, q34, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, " ", null));

        automaton13 = q30;

        //Rule 14:
        IState q35 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q36 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q37 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q38 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q39 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q35, q35, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "m"));
        LinkManager.getInstance().addLink(q35, q36, PropositionFactory.getInstance().createProposition("m", null));

        LinkManager.getInstance().addLink(q36, q35, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "t"));
        LinkManager.getInstance().addLink(q36, q37, PropositionFactory.getInstance().createProposition("t", null));

        LinkManager.getInstance().addLink(q37, q35, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "s"));
        LinkManager.getInstance().addLink(q37, q38, PropositionFactory.getInstance().createProposition("s", null));

        LinkManager.getInstance().addLink(q38, q39, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, " ", null));

        automaton14 = q35;

        //Rule 18:
        IState q40 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q41 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q42 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q43 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q44 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q40, q40, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "m"));
        LinkManager.getInstance().addLink(q40, q41, PropositionFactory.getInstance().createProposition("m", null));

        LinkManager.getInstance().addLink(q41, q40, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "t"));
        LinkManager.getInstance().addLink(q41, q42, PropositionFactory.getInstance().createProposition("t", null));

        LinkManager.getInstance().addLink(q42, q40, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "r"));
        LinkManager.getInstance().addLink(q42, q43, PropositionFactory.getInstance().createProposition("r", null));

        LinkManager.getInstance().addLink(q43, q44, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, " ", null));

        automaton18 = q40;

        //Rule 15:
        IState q45 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q46 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q47 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q48 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q49 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q45, q45, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, " ", null));
        LinkManager.getInstance().addLink(q45, q46, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, "", null));

        LinkManager.getInstance().addLink(q46, q45, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, " ", null));
        LinkManager.getInstance().addLink(q46, q47, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, "", null));

        LinkManager.getInstance().addLink(q47, q45, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, " ", null));
        LinkManager.getInstance().addLink(q47, q48, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, "", null));

        LinkManager.getInstance().addLink(q48, q45, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, " ", null));
        LinkManager.getInstance().addLink(q48, q49, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, "", null));

        automaton15 = q45;

        //Rule 15:
        IState q50 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q51 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q52 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q53 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q54 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q50, q50, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, " ", null));
        LinkManager.getInstance().addLink(q50, q51, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, "", null));

        LinkManager.getInstance().addLink(q51, q50, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, " ", null));
        LinkManager.getInstance().addLink(q51, q52, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, "", null));

        LinkManager.getInstance().addLink(q52, q50, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, " ", null));
        LinkManager.getInstance().addLink(q52, q53, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, "", null));

        LinkManager.getInstance().addLink(q53, q50, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, " ", null));
        LinkManager.getInstance().addLink(q53, q54, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, "", null));

        automaton16 = q50;

        //Rule 17A:
        IState q55 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q56 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q57 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q55, q55, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "d"));
        LinkManager.getInstance().addLink(q55, q56, PropositionFactory.getInstance().createProposition("d", null));

        LinkManager.getInstance().addLink(q56, q55, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, "r", null));
        LinkManager.getInstance().addLink(q56, q57, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, null, "r"));

        automaton17A = q55;

        //Rule 17B:
        IState q58 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q59 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q60 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q58, q58, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "t"));
        LinkManager.getInstance().addLink(q58, q59, PropositionFactory.getInstance().createProposition("t", null));

        LinkManager.getInstance().addLink(q59, q58, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, "rs", null));
        LinkManager.getInstance().addLink(q59, q60, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, null, "rs"));

        automaton17B = q58;

        //Rule 17B:
        IState q61 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q62 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q63 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q61, q61, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "m"));
        LinkManager.getInstance().addLink(q61, q62, PropositionFactory.getInstance().createProposition("m", null));

        LinkManager.getInstance().addLink(q62, q61, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, "bp", null));
        LinkManager.getInstance().addLink(q62, q63, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, null, "bp"));

        automaton17C = q61;

        //Rule 17D:
        IState q64 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q65 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q66 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q64, q64, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "n"));
        LinkManager.getInstance().addLink(q64, q65, PropositionFactory.getInstance().createProposition("n", null));

        LinkManager.getInstance().addLink(q65, q64, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_VOWELS, "kjtdg", null));
        LinkManager.getInstance().addLink(q65, q66, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY_CONSONANTS, null, "kjtdg"));

        automaton17D = q64;

        //Rule 24EXT
        IState q67 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q68 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q69 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q67, q67, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "e"));
        LinkManager.getInstance().addLink(q67, q68, PropositionFactory.getInstance().createProposition("e", null));

        LinkManager.getInstance().addLink(q68, q67, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "i"));
        LinkManager.getInstance().addLink(q68, q69, PropositionFactory.getInstance().createProposition("i", null));

        automaton24EXT = q67;

        //Rule 25EXT
        IState q70 = StateFactory.getInstance().createState(EnumState.FINAL_STATE);
        IState q71 = StateFactory.getInstance().createState(EnumState.STATE);
        IState q72 = StateFactory.getInstance().createState(EnumState.WELL);

        LinkManager.getInstance().addLink(q70, q70, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "i"));
        LinkManager.getInstance().addLink(q70, q71, PropositionFactory.getInstance().createProposition("i", null));

        LinkManager.getInstance().addLink(q71, q70, PropositionFactory.getInstance().createProposition(EnumLang.MALAGASY, null, "y"));
        LinkManager.getInstance().addLink(q71, q72, PropositionFactory.getInstance().createProposition("y", null));

        automaton25EXT = q70;
    }    

    /**
     * Obtenir une instance d'un automate fini correspondant a un règle de graphie
     * de la langue malagasy
     * @param i
     * Numéro du règle de graphie (voir document des règles de graphies)
     * @return
     * Retourne une instance de l'état initial de l'automate ou retourne un état
     * de type puits(error) si le numéro n'éxiste pas
     */
    public IState createAutomaton(EnumMalagasyAutomaton i) {
        switch(i) {
            //Rule 1
            case Q1: return automaton1;

            //Rule 4
            case Q4: return automaton4;

            //Rule 5
            case Q5: return automaton5;

            //Rule 6,8
            case Q6_8: return automaton6_8;

            case Q7_9_10_11_12: return automaton7_9_10_11_12;

            //Rule 13:
            case Q13: return automaton13;

            //Rule 14:
            case Q14: return automaton14;

            //Rule 15
            case Q15: return automaton15;

            //Rule 16
            case Q16: return automaton16;

            //Rule 17
            case Q17A: return automaton17A;
            case Q17B: return automaton17B;
            case Q17C: return automaton17C;
            case Q17D: return automaton17D;

            //Rule 18:
            case Q18: return automaton18;

            //Rule 19
            case Q19: return automaton19;

            //Rule 21
            case Q21: return automaton21;

            //Rule 23
            case Q23: return automaton23;

            //Rule 24Ext
            case Q24EXT: return automaton24EXT;

            //Rule 25Ext
            case Q25EXT: return automaton25EXT;
        }

        return new Well("Error");
    }
}