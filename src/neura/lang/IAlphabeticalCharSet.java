package neura.lang;

/**
 *
 * @author Mitanjo
 * Interface a implémenter pour les ensembles de caractères d'un alpbahet d'une langue
 */
public interface IAlphabeticalCharSet {
    /**
     * Retourne un sous-ensemble de caractères(CharSet) consonnes
     * @return
     * Ensemble de caractères(CharSet)
     */
    public ICharSet getConsonants();

    /**
     * Retourne un sous-ensemble de caractères(CharSet) voyelles
     * @return
     * Ensemble de caractères(CharSet)
     */
    public ICharSet getVowels();
}
