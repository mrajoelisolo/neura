package neura.lang;

/**
 *
 * @author Mitanjo
 * Elle est utilisé pour créer un ensemble de caractères(CharSet) et possède une
 * méthode pour vérifier si un caractère donné appartient ou non a l'ensemble
 */
public interface ICharSet {
    /**
     * Méthode pour vérifier si le caractère spécifié appartient ou non
     * a l'ensemble de valeurs que le CharSet contient
     * @param c
     * Le caractère a vérifier
     * @param additional
     * Autres caractères qui ne sont pas dans l'ensemble de valeurs mais
     * qui doivent êtres verifiées
     * @param except
     * Caractères qui ne doivent pas être vérifiées dans l'ensemble de valeurs,
     * donc a exclure
     * @return
     * Retourne une valeur vraie si le caractère appartient a l'ensemble
     * sinon retourne une valeur fausse
     */
    public boolean contains(String c, String additional, String except);

    /**
     * Retourne l'ensemble de valeur du CharSet
     * @return
     * Chaîne de caractères
     */
    public String getValue();
}