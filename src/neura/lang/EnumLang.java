package neura.lang;

/**
 *
 * @author Mitanjo
 */
public enum EnumLang {
    MALAGASY, MALAGASY_CONSONANTS, MALAGASY_VOWELS
}
