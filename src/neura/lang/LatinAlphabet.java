package neura.lang;

import java.io.Serializable;

/**
 *
 * @author Mitanjo
 * Ensemble de caractères contenant l'alphabet latin, ainsi que les sous ensembles
 * de consonnes(consonants) et de voyelles(vowels)
 */
public class LatinAlphabet implements ICharSet, IAlphabeticalCharSet, Serializable {
    private static LatinAlphabet instance = new LatinAlphabet();

    private LatinAlphabet() {}

    public static LatinAlphabet getInstance() {
        return instance;
    }

    private CharSet fullAlphabet = new CharSet("abcdefghijklmnopqrstuvwxyz");
    private CharSet consonants = new CharSet("bcdefghijklmnpqrstvwxz");
    private CharSet vowels = new CharSet("aeiouy");
    
    public CharSet getConsonants() {
        return consonants;
    }
    
    public CharSet getVowels() {
        return vowels;
    }

    public boolean contains(String c, String additional, String except) {
        return fullAlphabet.contains(c, additional, except);
    }

    public String getValue() {
        return fullAlphabet.getValue();
    }
}
