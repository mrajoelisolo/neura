package neura.lang;

import java.io.Serializable;

/**
 *
 * @author Mitanjo
 * Ensemble de valeurs que possède une proposition
 * Cette classe possède des méthodes pour vérifier si une caractère appartient
 * ou non a l'ensemble de valeurs; elle est utilisé pour vérifier une proposition
 */
public class CharSet implements ICharSet, Serializable {
    private String value;

    public CharSet(String value) {
        this.value = value;
    }

    /**
     * Méthode pour vérifier si le caractère spécifié appartient ou non
     * a l'ensemble de valeurs que le CharSet contient
     * @param c
     * Le caractère a vérifier
     * @return
     * Retourne une valeur vraie si le caractère appartient a l'ensemble
     * sinon retourne une valeur fausse
     */
    public boolean contains(String c)
    {
        return value.contains(c);
    }

    /**
     * Méthode pour vérifier si le caractère spécifié appartient ou non
     * a l'ensemble de valeurs que le CharSet contient
     * @param c
     * Le caractère a vérifier
     * @param except
     * Caractères qui ne doivent pas être vérifiées dans l'ensemble de valeurs,
     * donc a exclure
     * @return
     * Retourne une valeur vraie si le caractère appartient a l'ensemble
     * sinon retourne une valeur fausse
     */
    public boolean contains(String c, String except) {
        String str = value;
        for(int i = 0; i < except.length(); i++) {
            str = str.replaceAll(except.substring(i, i+1), "");
        }

        return str.contains(c);
    }

    public boolean contains(String c, String additional, String except) {
        String str = value + additional;
        for(int i = 0; i < except.length(); i++) {
            str = str.replaceAll(except.substring(i, i+1), "");
        }

        return str.contains(c);
    }

    /**
     * Méthode permettant de vérifier si l'ensemble de caractères (CharSet)
     * ont le même ensemble de caractères spécifié
     * @param value
     * Ensemble de caractères a vérifier
     * @return
     * Retourne une valeur vraie si les deux ensembles sont les mêmes
     * sinon retourne une valeur fausse
     */
    public boolean isEquivalent(String value) {
        return (this.value.replace(value, "").length() == 0);
    }

    public String getValue() {
        return value;
    }
}
