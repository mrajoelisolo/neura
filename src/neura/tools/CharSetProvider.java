package neura.tools;

import java.util.ArrayList;
import java.util.List;
import neura.lang.CharSet;
import neura.lang.ICharSet;

/**
 *
 * @author Mitanjo
 * Classe permettant d'obtenir un ensemble de caractères(CharSet)
 * Si l'ensemble de caractères éxiste déja, elle le retourne
 * sinon elle en crée un nouveau
 */
public class CharSetProvider {
    private static CharSetProvider instance = new CharSetProvider();

    private CharSetProvider() {}

    public static CharSetProvider getInstance() {
        return instance;
    }

    private List<CharSet> charSets = new ArrayList();

    /**
     * Retourne une instance d'un ensemble de caractères(CharSet)
     * @param value
     * Ensemble de caractères à obtenir
     * @return
     * Instance d'un ensemble de caractères
     */
    public ICharSet getCharSet(String value) {
        for(CharSet charSet : charSets) {
            if(charSet.isEquivalent(value))
                return charSet;
        }

        CharSet newCharSet = new CharSet(value);
        charSets.add(newCharSet);
        return newCharSet;
    }
}
