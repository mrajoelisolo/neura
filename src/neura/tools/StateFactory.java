package neura.tools;

import neura.states.EnumState;
import neura.states.IState;
import neura.states.Well;
import neura.states.State;

/**
 *
 * @author Mitanjo
 * Classe permettant de générer une instance d'état à partir des valeurs spécifiées
 */
public class StateFactory {
    private static StateFactory instance = new StateFactory();

    private StateFactory() {}

    public static StateFactory getInstance() {
        return instance;
    }

    /**
     * Crée une instance d'état à partir d'un type d'état choisie
     * @param stateType
     * STATE, INITIAL_STATE, FINAL_STATE, WELL
     * @return
     * Instance d'un état ou valeur nulle
     */
    public IState createState(EnumState stateType) {
        IState res = null;

        if(stateType == EnumState.STATE || stateType == EnumState.INITIAL_STATE || stateType == EnumState.FINAL_STATE)
            res = new State("q" + StateIndexer.getInstance().getCurrentIndex(), stateType);
        else if(stateType == EnumState.WELL)
            res = new Well("q" + StateIndexer.getInstance().getCurrentIndex());

        return res;
    }
}
