package neura.tools;

import neura.states.Link;
import neura.states.Proposition;
import neura.states.IState;
import java.util.List;

/**
 *
 * @author Mitanjo
 * Classe permettant de trouver l'arc correspondant au caractère spécifié
 */
public class Pathfinder {
    private static Pathfinder instance = new Pathfinder();

    private Pathfinder() {}

    public static Pathfinder getInstance() {
        return instance;
    }

    /**
     * Méthode permettant de retourner l'état designé par l'arc correspondant
     * au caractère spécifié
     * @param c
     * Caractère a vérifier
     * @param links
     * Ensemble des arcs a vérifier
     * @return
     * Instance d'un état ou valeur nulle
     */
    public IState findDestination(String c, List<Link> links) {
        for(Link link : links) {
            Proposition proposition = link.getProposition();

            //System.out.println("Link : " + link.toString() + " Proposition : " + proposition.test(c));

            if(proposition.test(c))
                return link.getDestination();
        }

        return null;
    }
}
