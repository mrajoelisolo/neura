package neura.tools;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import neura.states.State;

/**
 *
 * @author Mitanjo
 */
public class StateSerializer {
    private static StateSerializer instance = new StateSerializer();

    private StateSerializer() {}

    public static StateSerializer getInstance() {
        return instance;
    }

    private static final String extension = ".dfa";

    public void saveState(State initialState, String path) {
        try {
            FileOutputStream file = new FileOutputStream(path);
            ObjectOutputStream oos = new ObjectOutputStream(file);
            oos.writeObject(initialState);
            oos.flush();
            oos.close();
        }catch(IOException e) {
            System.err.println("Internal error on " + this.getClass().getSimpleName() + " :" + e);
        }
    }

    public State loadState(String path) {
        State res = null;

        try {
            FileInputStream file = new FileInputStream(path);
            ObjectInputStream ois = new ObjectInputStream(file);
            res = (State)ois.readObject();
        }catch(IOException e) {
            System.err.println("Internal error on " + this.getClass().getSimpleName() + " :" + e);
        }catch(ClassNotFoundException e) {
           System.err.println("Internal error on " + this.getClass().getSimpleName() + " :" + e);
        }

        return res;
    }
}
