package neura.tools;

import neura.states.Proposition;
import neura.states.IState;
import neura.states.State;

/**
 *
 * @author Mitanjo
 * Classe permettant de lier deux états par un arc(Link)
 */
public class LinkManager {
    private static LinkManager instance = new LinkManager();

    public LinkManager() {}

    public static LinkManager getInstance() {
        return instance;
    }

    /**
     * Méthode permettant de lier deux états par un Link, défini par une proposition
     * @param state
     * Etat de départ correspondant a l'arc sortant
     * Note: Un état de type puits(ou well) ne peut pas être définie comme état de départ
     * @param target
     * Etat destination, correspondant a l'arc entrant
     * @param proposition
     * Proposition correspondant à l'arc liant les deux états
     */
    public void addLink(IState state, IState target, Proposition proposition) {
        if(!(state instanceof State)) return;

        State st = (State) state;
        st.addLink(target, proposition);
    }
}
