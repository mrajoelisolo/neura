package neura.tools;

import neura.states.EnumState;
import neura.states.IState;

/**
 *
 * @author Mitanjo
 * Classe permettant de lancer un processus à partir d'un automate fini
 */
public class StateRunner {
    private static StateRunner instance = new StateRunner();

    private StateRunner() {}

    public static StateRunner getInstance() {
        return instance;
    }

    /**
     * Lance le processus à partir d'un automate
     * @param initialState
     * Spécifier l'état initial
     * @param word
     * Môt a vérifier par l'automate
     * @return
     * Type de la dernière état
     */
    public EnumState runState(IState initialState, String word) {
        EnumState stateResult = EnumState.UNDEFINED;

        String str = word.toLowerCase() + " ";

        IState pState = initialState;
        if(pState != null)
            for(int i = 0; i < str.length(); i++) {                
                String c = str.substring(i, i+1);             

                IState next = pState.next(c);
                if(next == null) break;
                pState = next;
                //System.out.println("" + pState.getName() + "[" + c + "]");
                stateResult = pState.getStateType();
                if(pState.getStateType() == EnumState.WELL) return EnumState.WELL;
            }

        return stateResult;
    }
}
