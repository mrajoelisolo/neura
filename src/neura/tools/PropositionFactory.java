package neura.tools;

import neura.states.Proposition;
import neura.lang.*;
import neura.lang.malagasy.MalagasyAlphabet;

/**
 *
 * @author Mitanjo
 * Classe permettant de générer une instance d'une proposition
 */
public class PropositionFactory {
    private static PropositionFactory instance = new PropositionFactory();

    private PropositionFactory() {}

    public static PropositionFactory getInstance() {
        return instance;
    }

    /**
     * Créer une proposition à partir des alphabets d'une langue donnée
     * @param lang
     * Type de la langue
     * @param additional
     * Ensemble de caractères supplémentaires dans la proposition
     * @param except
     * Ensemble de caractères a exclure dans la proposition
     * @return
     * Instance d'une proposition correspondant à la langue spécifié
     */
    public Proposition createProposition(EnumLang lang, String additional, String except) {
        switch(lang) {
            case MALAGASY : {
                return new Proposition(MalagasyAlphabet.getInstance(), additional, except);
            }

            case MALAGASY_CONSONANTS : {
                return new Proposition(MalagasyAlphabet.getInstance().getConsonants(), additional, except);
            }

            case MALAGASY_VOWELS : {
                return new Proposition(MalagasyAlphabet.getInstance().getVowels(), additional, except);
            }
        }

        return new Proposition(LatinAlphabet.getInstance(), additional, except);
    }

    /**
     * Crée une proposition à partir d'un ensemble de caractères(CharSet) personnalisées
     * @param str
     * Ensemble de caractères a spécifier dans la proposition
     * @param except
     * Ensemble de caractères a exclure dans la proposition
     * @return
     * Instance d'une proposition
     */
    public Proposition createProposition(String str, String except) {
        return new Proposition(new CharSet(str), null, except);
    }
}
