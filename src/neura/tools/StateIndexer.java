package neura.tools;

/**
 *
 * @author Mitanjo
 * Classe permettant de numéroter automatiquement un état
 */
public class StateIndexer {
    private static StateIndexer instance = new StateIndexer();

    private StateIndexer() {}

    public static StateIndexer getInstance() {
        return instance;
    }

    private int currentIndex = 0;

    public int getCurrentIndex() {
        return currentIndex++;
    }
}
