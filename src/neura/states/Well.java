package neura.states;

import java.io.Serializable;

/**
 *
 * @author Mitanjo
 * Etat de type puits ou well, elle ne possède pas d'arcs sortants mais elle
 * peut avoir un ou plusieurs arcs entrants
 */
public class Well implements IState, Serializable {
    private String name;

    public Well(String name) {
        this.name = name;
    }

    public IState next(String c) {
        return null;
    }

    public EnumState getStateType() {
        return EnumState.WELL;
    }

    public String getName() {
        return name;
    }
}
