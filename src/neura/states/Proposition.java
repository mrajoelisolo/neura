package neura.states;

import java.io.Serializable;
import neura.lang.*;

/**
 *
 * @author Mitanjo
 * Une proposition est rattaché a un arc, elle contient un ensemble de valeurs
 * qui sera vérifié pour trouver la l'arc qui vérifie un caractère et obtenir l'état suivant
 */
public class Proposition implements Serializable {
    private ICharSet charSet;
    private String additional;
    private String except;

    public Proposition(ICharSet charSet, String additional, String except) {
        this.charSet = charSet;
        this.additional = additional;
        this.except = except;

        if(additional == null) this.additional = "";
        if(except == null) this.except = "";
    }

    /**
     * Tester la proposition correspondant a un arc si le cacactère spécifié
     * appartient a l'ensemble de valeurs de la proposition
     * @param c
     * Caractère unique
     * @return
     * Si le caractère spécifié correspond a l'arc, retourne une valeur vraie
     * sinon retourne une valeur fausse
     */
    public boolean test(String c) {        
        return charSet.contains(c, additional, except);
    }
}