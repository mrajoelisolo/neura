package neura.states;

/**
 *
 * @author Mitanjo
 */
public enum EnumState {
    STATE, WELL, INITIAL_STATE, FINAL_STATE, UNDEFINED
}
