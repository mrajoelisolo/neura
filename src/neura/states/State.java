package neura.states;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import neura.tools.*;

/**
 *
 * @author Mitanjo
 * Etat de type normal, initial ou final
 * Il peut avoir plusieurs arcs sortants qui sont reliés chacun a d'autres etats
 * L'etat peut être de type normal, final ou initial
 */
public class State implements IState, Serializable {
    private List<Link> destinations = new ArrayList();
    private String name;
    private EnumState stateType = EnumState.STATE;

    public State(String name) {
        this.name = name;
    }

    public State(String name, EnumState stateType) {
        this(name);
        this.stateType = stateType;
    }

    public String getName() {
        return name;
    }

    /**
     * Ajouter un arc sortant a l'état correspondant
     * @param target
     * Etat suivant designé par l'arc sortant
     * @param proposition
     * Proposition correspondant a l'arc
     */
    public void addLink(IState target, Proposition proposition) {
        destinations.add(new Link(target, proposition));
    }

    public IState next(String c) {
        return Pathfinder.getInstance().findDestination(c, destinations);
    }

    public EnumState getStateType() {
        return stateType;
    }
}
