package neura.states;

/**
 *
 * @author Mitanjo
 * Interface a implémenter pour les classes de type 'états' tel que les états et les puits (well)
 */
public interface IState {
    /**
     * Obtenir l'état ou noeud suivant correspondant a l'arc vérifiant la valeur de c
     * @param c
     * Caractère du môt a traiter
     * @return
     * Instance d'un etat ou valeur nulle
     */
    public IState next(String c);

    /***
     * Obtenir le type de l'état correspondant (état normal, initial ou final ou puits ...)
     * @return
     * Instance d'un état ou valeur nulle
     */
    public EnumState getStateType();

    /***
     * Obtenir le nom de l'état correspondant, préfixé par la lettre 'q' ex: q1, q2 ...
     * @return
     * Type de l'état correspondant (STATE, INITIAL_STATE, FINAL_STATE)
     */
    public String getName();
}
