package neura.states;

import java.io.Serializable;

/**
 *
 * @author Mitanjo
 * La classe link peut être considéré comme une transion ou arc dans un graphe orienté
 * auquel il relie deux noeuds ou états
 */
public class Link implements Serializable {
    private IState destination;
    private Proposition proposition;

    public Link(IState destination, Proposition proposition) {
        this.destination = destination;
        this.proposition = proposition;
    }    

    /**
     * Obtenir l'état designé par l'arc
     * @return
     * Instance d'un état ou valeur nulle
     */
    public IState getDestination() {
        return destination;
    }

    /**
     * Obtenir la proposition correspondant à l'arc
     * @return
     * Instance d'une proposition ou valeur nulle
     */
    public Proposition getProposition() {
        return proposition;
    }
}
